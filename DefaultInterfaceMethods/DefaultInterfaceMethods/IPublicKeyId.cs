﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DefaultInterfaceMethods
{
    public interface IPublicKeyId
    {
        int Id { get; set; }

        //Default interface method
        Guid PublicKey() { return Guid.NewGuid(); }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DefaultInterfaceMethods
{
    public interface IPublicKeyIdOverride : IPublicKeyId
    {
        //Default interface method override
        Guid IPublicKeyId.PublicKey() { return Guid.Empty; }
    }
}

﻿using System;

namespace DefaultInterfaceMethods
{
    class Program
    {
        static void Main(string[] args)
        {
            var pid = new PublicKeyId() as IPublicKeyId;
            Console.WriteLine($"default interface method public key={pid.PublicKey()}");

            var pidOverride = new PublicKeyId() as IPublicKeyIdOverride;
            Console.WriteLine($"detault interface method override public key={pidOverride.PublicKey()}");
        }
    }
}

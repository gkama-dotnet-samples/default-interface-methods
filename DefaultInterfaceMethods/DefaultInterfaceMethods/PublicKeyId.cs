﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DefaultInterfaceMethods
{
    public class PublicKeyId : IPublicKeyId, IPublicKeyIdOverride
    {
        public int Id { get; set; }
    }
}
